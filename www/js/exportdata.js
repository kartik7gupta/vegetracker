function exportUserDetails() {
    var ref = new Firebase('https://vege-tracker.firebaseio.com');

    var email, username, dob, age, gender, gamification, startDate, currentStage, dailyServesGoal, timesGoalReached;
    var now = new Date();
    var currentYear = now.getFullYear();
    var data = [];
    var columns = ["uid", "email", "username", "dob", "age", "gender", "gamification", "currentStage", "dailyServesGoal", "timesGoalReached"];
    data.push(columns);
    ref.once('value', function (snapshot) {
        snapshot.forEach(function (func) {
            var csvEntry = [];
            var uid = func.key();
            email = func.child("userDetails").child("email").val();
            username = func.child("userDetails").child("username").val();
            dob = func.child("userDetails").child("dob").val();
            age = currentYear - new Date(dob).getFullYear();
            gender = func.child("userDetails").child("gender").val();
            gamification = func.child("userDetails").child("gamification").val();
            currentStage = func.child("userDetails").child("currentStage").val();
            dailyServesGoal = func.child("userDetails").child("dailyServesGoal").val();
            timesGoalReached = func.child("userDetails").child("timesGoalReached").val();
            csvEntry.push(uid);
            csvEntry.push(email);
            csvEntry.push(username);
            csvEntry.push(dob);
            csvEntry.push(age);
            csvEntry.push(gender);
            csvEntry.push(gamification);
            csvEntry.push(currentStage);
            csvEntry.push(dailyServesGoal);
            csvEntry.push(timesGoalReached);
            data.push(csvEntry);
        });
        var file = convertArrayOfObjectsToCSV({data: data});
        downloadCSV({filename: "VegeTracker-UserDetails.csv"}, file);
    })
}

function exportUserStatistics() {
    var ref = new Firebase('https://vege-tracker.firebaseio.com');
    var username, uid, email, age, gender, gamification;
    var data = [];
    var columns =
        [
            "uid", "email", "age", "gender", "gamification", "date", "goal", "reachGoal", "servesEaten", "mealtime",
            "serve", "brassica", "green", "potatoes", "peas", "other", "root", "time"
        ];
    data.push(columns);

    ref.once('value', function (snapshot) {

        snapshot.forEach(function (userObject) {

            var statsObject = userObject.child("statistic").val();
            uid = userObject.key();
            username = userObject.child("userDetails").child("username").val();
            email = userObject.child("userDetails").child("email").val();
            var dob = userObject.child("userDetails").child("dob").val();
            gender = userObject.child("userDetails").child("gender").val();
            var now = new Date();
            var currentYear = now.getFullYear();
            age = currentYear - new Date(dob).getFullYear();
            gamification = userObject.child("userDetails").child("gamification").val();
            var csventry = [];

            if (statsObject == null) {
                csventry.push(uid);
                csventry.push(email);
                csventry.push(age);
                csventry.push(gender);
                csventry.push(gamification);
                data.push(csventry);

            } else {
                for (var eachDay in statsObject) {
                    var goal = statsObject[eachDay]["goal"];
                    var reachGoal = statsObject[eachDay]["reachGoal"];
                    var servesEaten = statsObject[eachDay]["servesEaten"];

                    if (statsObject[eachDay] == null) {
                        csventry = [];
                        csventry.push(uid);
                        csventry.push(email);
                        csventry.push(age);
                        csventry.push(gender);
                        csventry.push(gamification);
                        csventry.push(eachDay);
                        data.push(csventry);

                    } else {
                        for (var item in statsObject[eachDay]) {
                            var meals = ["Breakfast", "Lunch", "Dinner", "Snack"];
                            var irrelevent = ["goal", "reachGoal", "servesEaten"];
                            if ((meals.indexOf(item) > -1) && (irrelevent.indexOf(item) == -1)) {
                                csventry = [];
                                csventry.push(uid);
                                csventry.push(email);
                                csventry.push(age);
                                csventry.push(gender);
                                csventry.push(gamification);
                                csventry.push(eachDay);
                                csventry.push(goal);
                                csventry.push(reachGoal);
                                csventry.push(servesEaten);
                                csventry.push(item);
                                csventry.push(statsObject[eachDay][item]["serve"]);
                                csventry.push(statsObject[eachDay][item]["brassica"]);
                                csventry.push(statsObject[eachDay][item]["green"]);
                                csventry.push(statsObject[eachDay][item]["potatoes"]);
                                csventry.push(statsObject[eachDay][item]["peas"]);
                                csventry.push(statsObject[eachDay][item]["other"]);
                                csventry.push(statsObject[eachDay][item]["root"]);

                                var systemtime = statsObject[eachDay][item].time;
                                var date = new Date(systemtime);
                                var hours = date.getHours();
                                var minutes = "0" + date.getMinutes();
                                var seconds = "0" + date.getSeconds();
                                var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

                                csventry.push(formattedTime);
                                data.push(csventry);
                            }
                        }
                    }
                }
            }
        });
        var file = convertArrayOfObjectsToCSV({data: data});
        downloadCSV({filename: "statistic-data.csv"}, file);
    });
}

function exportUserInteractions() {
    var ref = new Firebase('https://vege-tracker.firebaseio.com');

    var uid, email, id, btnId, time;

    var data = [];
    var columns =
        [
            "uid", "email", "btnId", "time", "date"
        ];

    data.push(columns);
    ref.once('value', function (snapshot) {
        snapshot.forEach(function (userObject) {
            var interactionTableObj = userObject.child("interactionTable").val();

            for (var obj in interactionTableObj) {
                var csvEntry = [];
                btnId = (interactionTableObj[obj.toString()]["btnId"]);
                time = (interactionTableObj[obj.toString()]["time"]);
                var date = new Date(time);
                var hours = date.getHours();
                var minutes = "0" + date.getMinutes();
                var seconds = "0" + date.getSeconds();
                var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
                var formattedDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

                csvEntry.push(uid);
                csvEntry.push(email);
                csvEntry.push(btnId);
                csvEntry.push(formattedTime);
                csvEntry.push(formattedDate);
                data.push(csvEntry);
            }

        });

        setTimeout(function () {
            var file = convertArrayOfObjectsToCSV({data: data});
            downloadCSV({filename: "VegeTracker-InteractionTable.csv"}, file);
        }, 5000);

    })

};

function convertArrayOfObjectsToCSV(args) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;
    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';
    keys = Object.keys(data[0]);
    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function (item) {
        ctr = 0;
        keys.forEach(function (key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
}

function downloadCSV(args, csv) {
    var data, filename, link;
    if (csv == null) {
        return;
    }
    filename = args.filename || 'data.csv';
    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);
    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
}