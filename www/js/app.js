// Ionic Starter App

angular.module('starter', ['ionic', 'ionic.service.core', 'starter.controllers', 'starter.services', 'tc.chartjs'])

    .run(function ($ionicPlatform, $rootScope) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }

            var push = new Ionic.Push({
                "debug": true
            });

            push.register(function (token) {
                console.log("Device token:", token.token);
                push.saveToken(token);  // persist the token in the Ionic Platform
            });

        });

        $rootScope.firebaseRef = new Firebase('https://vege-tracker.firebaseio.com');

        $rootScope.firebaseRef.child("gamification").once("value", function (snapshot) {
            $rootScope.last_male = snapshot.child("Male").child("count").val();
            $rootScope.last_female = snapshot.child("Female").child("count").val();


            //console.log($rootScope.last_male);
            //console.log($rootScope.last_female);

        });


        $rootScope.logBtnClick = function (btnId) {
            console.log("Logging Click: " + btnId);
            var firebaseRef = new Firebase('https://vege-tracker.firebaseio.com');
            var currentUser = localStorage.getItem("currentUser");

            var userObject = firebaseRef.child(currentUser);
            var newStat = userObject.child("interactionTable").push({

                time: Firebase.ServerValue.TIMESTAMP,
                btnId: btnId

            }, function (error) {
                if (error) {
                    console.log(error);
                }
            });
        };
    })

    .config(function ($ionicConfigProvider, $stateProvider, $urlRouterProvider) {

        $ionicConfigProvider.tabs.position('bottom');

        $stateProvider

            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })

            .state('tab.account', {
                url: '/account',
                views: {
                    'tab-account': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            })

            .state('tab.dashboard', {
                url: '/dashboard',
                views: {
                    'tab-dashboard': {
                        templateUrl: 'templates/tab-dashboard.html',
                        controller: 'DashboardCtrl'
                    }
                }
            })

            .state('tab.log', {
                url: '/log',
                views: {
                    'tab-log': {
                        templateUrl: 'templates/tab-log.html',
                        controller: 'LogCtrl'
                    }
                }
            })

            .state('tab.update', {
                url: '/update',
                views: {
                    'tab-update': {
                        templateUrl: 'templates/tab-update.html',
                        controller: 'UpdateCtrl'
                    }
                }
            })

            .state('tab.history', {
                url: '/history',
                views: {
                    'tab-history': {
                        templateUrl: 'templates/tab-history.html',
                        controller: 'HistoryCtrl'
                    }
                }
            })

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'

            })

            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'LoginCtrl'
            })

            .state('rewards', {
                url: '/rewards',
                templateUrl: 'templates/rewards.html',
                controller: 'RewardCtrl'
            })

            .state('vegetableTypes', {
                url: 'vegetableTypes',
                templateUrl: 'templates/vegetableTypes.html',
                controller: 'VegetableTypesCtrl'
            })

            .state('admin', {
                url: '/admin',
                templateUrl: 'templates/admin.html',
                controller: 'AdminCtrl'
            });

        $urlRouterProvider.otherwise('/login');

    });