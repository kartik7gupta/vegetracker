/*
 * Returns number to one decimal point
 */
function oneDecimal(number) {
    return Number(number * 1).toFixed(1);
}

/*
 * Confirm the intake.
 * Add timestamp and amount to Firebase
 */
function confirmIntake(firebaseRef, currentUser, todayDate, mealTime, serve, vegetableType, isUpdateFlag, $state) {
    var userObject = firebaseRef.child(currentUser);
    var newStat = userObject.child("statistic").child(todayDate).push({}, function (error) {
        if (error) {
            console.log(error);
        }
        if (!error) {
            if (isUpdateFlag) {
                updateIntake(userObject, todayDate, mealTime, serve, vegetableType, newStat.key(), $state);

            } else {
                logIntake(userObject, todayDate, mealTime, serve, vegetableType, newStat.key(), $state);
            }
        }
    })
}

/*
 * Writes the User's Intake to Firebase in the format:
 * Today's Date -> Unique Key -> MealTime:
 * Timestamp, Serve, [Vegetables Type - Boolean]
 * If writing to DB is successful;
 * Status is Updated
 */
function logIntake(userObject, todayDate, mealTime, serve, vegetableType, key, $state) {
    userObject.once("value", function (snapshot) {
        var alreadyEaten = snapshot.child("statistic").child(todayDate).child(mealTime).child("serve").val();

        var green = (snapshot.child("statistic").child(todayDate).child(mealTime).child("green").val() === true);
        var peas = (snapshot.child("statistic").child(todayDate).child(mealTime).child("peas").val() === true);
        var potatoes = (snapshot.child("statistic").child(todayDate).child(mealTime).child("potatoes").val() === true);
        var root = (snapshot.child("statistic").child(todayDate).child(mealTime).child("root").val() === true);
        var brassica = (snapshot.child("statistic").child(todayDate).child(mealTime).child("brassica").val() === true);
        var other = (snapshot.child("statistic").child(todayDate).child(mealTime).child("other").val() === true);

        if (alreadyEaten == null) {
            alreadyEaten = 0;
        }

        userObject.child("statistic").child(todayDate).child(mealTime).update({
            time: Firebase.ServerValue.TIMESTAMP,
            serve: alreadyEaten + serve,
            green: (vegetableType[0].checked || green),
            peas: (vegetableType[1].checked || peas),
            potatoes: (vegetableType[2].checked || potatoes),
            root: (vegetableType[3].checked || root),
            brassica: (vegetableType[4].checked || brassica),
            other: (vegetableType[5].checked || other)
        }, function (error) {
            if (error) {
                console.log(error);
            } else {
                updateStatus(userObject, todayDate, serve, 0, false, $state);
            }
        });
    })
}

function updateIntake(userObject, todayDate, mealTime, serve, vegetableType, key, $state) {
    userObject.once("value", function (snapshot) {
        var alreadyEaten = snapshot.child("statistic").child(todayDate).child(mealTime).child("serve").val();
        var difference = serve - alreadyEaten;

        userObject.child("statistic").child(todayDate).child(mealTime).update({
            time: Firebase.ServerValue.TIMESTAMP,
            serve: serve,
            green: vegetableType[0].checked,
            peas: vegetableType[1].checked,
            potatoes: vegetableType[2].checked,
            root: vegetableType[3].checked,
            brassica: vegetableType[4].checked,
            other: vegetableType[5].checked
        }, function (error) {
            if (error) {
                console.log(error);
            } else {
                updateStatus(userObject, todayDate, serve, difference, true, $state);
            }
        });

    })
}

/*
 * Updates the Daily Goal, Current and reachGoal Stats
 */
function updateStatus(userObject, todayDate, serve, difference, isUpdate, $state) {
    userObject.once("value", function (snapshot) {

        var dailyServesGoal = snapshot.child("statistic").child(todayDate).child("goal").val();
        if (dailyServesGoal == null) {
            dailyServesGoal = snapshot.child("userDetails").child("dailyServesGoal").val();
        }

        var servesEaten = snapshot.child("statistic").child(todayDate).child("servesEaten").val();
        if (servesEaten == null) {
            servesEaten = 0;
        }
        if (isUpdate) {
            var servesEaten = servesEaten + difference;
        } else {
            var servesEaten = servesEaten + serve;
        }

        var reachGoal = snapshot.child("statistic").child(todayDate).child("reachGoal").val();
        if (reachGoal == null) {
            reachGoal = "false";
        }

        var timesGoalReached = snapshot.child("userDetails").child("timesGoalReached").val();

        if (reachGoal == "false" && servesEaten >= dailyServesGoal) {
            reachGoal = "true";
            userObject.child("userDetails").update({
                timesGoalReached: timesGoalReached + 1
            }, function (error) {
                if (error) {
                    console.log(error);
                }
            });
        } else if (reachGoal == "true" && servesEaten <= dailyServesGoal) {
            reachGoal = "false";
            userObject.child("userDetails").update({
                timesGoalReached: timesGoalReached - 1
            }, function (error) {
                if (error) {
                    console.log(error);
                }
            });
        }

        userObject.child("statistic").child(todayDate).update({
            goal: dailyServesGoal,
            servesEaten: servesEaten,
            reachGoal: reachGoal
        }, function (error) {
            if (error) {
                console.log(error);
            } else {
                $state.go('tab.dashboard');
            }
        });
    })
}
