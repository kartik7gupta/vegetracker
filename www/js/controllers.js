angular.module('starter.controllers', ['starter.services'])

    .controller('LoginCtrl', function ($rootScope, $scope, $ionicPopup, $state, GetTodayService) {

        if (localStorage.getItem("currentUser") != null) {
            $state.go('tab.dashboard');
        }

        var firebaseRef = $rootScope.firebaseRef;

        $scope.register = function () {

            //Fetch User Information
            var email = document.getElementById("email").value;
            var password = document.getElementById("password").value;
            var repassword = document.getElementById("repassword").value;
            var username = document.getElementById("username").value;
            var dob = document.getElementById("dob").value;
            var gender = document.getElementById("gender").value;

            //Input Validation
            if (email == "") {
                var myPopup = $ionicPopup.alert({
                    title: 'Correct Field',
                    template: 'Email cannot be empty'
                });
                return;
            }

            if (password == "") {
                var myPopup = $ionicPopup.alert({
                    title: 'Correct Field',
                    template: 'Password cannot be empty'
                });
                return;
            }

            if (password.value != repassword.value) {
                var myPopup = $ionicPopup.alert({
                    title: 'Correct Field',
                    template: 'Passwords do not match'
                });
                return;
            }

            if (username == "") {
                var myPopup = $ionicPopup.alert({
                    title: 'Correct Field',
                    template: 'Username cannot be empty'
                });
                return;
            }

            if (dob == "") {
                var myPopup = $ionicPopup.alert({
                    title: 'Correct Field',
                    template: 'Date of Birth cannot be empty'
                });
                return;
            }

            if (gender == "") {
                var myPopup = $ionicPopup.alert({
                    title: 'Correct Field',
                    template: 'Please choose Gender'
                });
                return;
            }

            var serves;
            if (gender == "Male") {
                serves = 6;
            } else {
                serves = 5;
            }

            /*
             * Creates a User with email and password,
             * Adds newUser, email, username, dob, gender,
             * dailyServesGoal, currentStage, timesGoalReached, startDate, gamification
             * dailyServesGoal = 6(men) OR 5 (women)
             * If successful redirects to tab.dashboard
             */
            firebaseRef.createUser({
                email: email,
                password: password

            }, function (error, userData) {
                if (error) {
                    console.log("Error creating user:", error);
                    switch (error.code) {
                        case "INVALID_EMAIL":
                            var myPopup = $ionicPopup.alert({
                                title: 'Invalid Email Used',
                                template: 'Please correct email to proceed'
                            });
                            break;
                        case "EMAIL_TAKEN":
                            var myPopup = $ionicPopup.alert({
                                title: 'Email address already registered',
                                template: 'Please use a different email address to proceed.'
                            });
                    }
                } else {
                    var currentUser = userData.uid;
                    localStorage.setItem("currentUser", currentUser);
                    var today = GetTodayService.ddmmyyyy();
                    var gamification = true;

                    firebaseRef.child(currentUser).set({
                        userDetails: {
                            newUser: "true",
                            email: email,
                            username: username,
                            dob: dob,
                            gender: gender,
                            dailyServesGoal: serves,
                            currentStage: 1,
                            timesGoalReached: 0,
                            startDate: today,
                            gamification: gamification
                        }

                    }, function (error) {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            $state.go('tab.dashboard');
                        }
                    });
                }
            });
        };

        $scope.login = function () {
            var email = document.getElementById("email_login").value;
            var password = document.getElementById("password_login").value;

            firebaseRef.authWithPassword({
                email: email,
                password: password

            }, function (error, authData) {
                if (error) {
                    var myPopup = $ionicPopup.alert({
                        title: 'Incorrect Email or Password',
                        template: 'Please check email address and password and try again'
                    });
                    console.log("Login Failed!", error);

                } else {
                    console.log("Authenticated successfully with payload:", authData);
                    localStorage.setItem("currentUser", authData.uid);

                    if (authData.uid == "8ff08725-2d61-42d3-a8b3-68ee39324a68") {
                        $state.go('admin');
                    } else {

                        $state.go('tab.dashboard');
                    }
                }

            });

        };

        $scope.forgotPassword = function () {
            $scope.data = {};
            var myPopup = $ionicPopup.show({
                template: '<input type="email" ng-model="data.email">',
                title: 'Enter email address',
                scope: $scope,
                buttons: [
                    {text: 'Cancel'},
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            if (!$scope.data.email) {
                                e.preventDefault();
                                var errPopup = $ionicPopup.alert({
                                    title: 'Invalid Email',
                                    template: 'Please enter a valid email address.'
                                });

                            } else {
                                firebaseRef.resetPassword({
                                    email: $scope.data.email
                                }, function (error) {
                                    if (error) {
                                        switch (error.code) {
                                            case "INVALID_USER":
                                                console.log("The specified user does not exist.");
                                                var errPopup = $ionicPopup.alert({
                                                    title: 'User Does Not Exist',
                                                    template: 'The specified user does not exist'
                                                });
                                                break;
                                            default:
                                                console.log("Error resetting password:", error);
                                                var errPopup = $ionicPopup.alert({
                                                    title: 'Password not reset',
                                                    template: 'Error resetting password'
                                                });
                                        }
                                    } else {
                                        console.log("Password reset email sent successfully!");
                                        var errPopup = $ionicPopup.alert({
                                            title: 'Password reset email sent successfully!',
                                            template: 'Please login with the password in your email.'
                                        });
                                    }
                                });
                            }
                        }
                    }
                ]
            });

        };
    })

    .controller('DashboardCtrl', function ($rootScope, $scope, $ionicPopup, $state, GetTodayService) {
        var firebaseRef = $rootScope.firebaseRef;
        var currentUser = localStorage.getItem("currentUser");
        var today = GetTodayService.ddmmyyyy();

        $scope.data = {};

        $scope.stages =
            [
                {
                    number: 1,
                    name: "Level 1",
                    nextStage: "Level 2",
                    description: "Signup and meet your daily serves goal",
                    current: false
                },
                {
                    number: 2,
                    name: "Level 2",
                    nextStage: "Level 3",
                    description: "Meet your serves goal for 2 days this week!",
                    current: false
                },
                {
                    number: 3,
                    name: "Level 3",
                    nextStage: "Level 4",
                    description: "Meet your serves goal for 4 days this week!",
                    current: false
                },
                {
                    number: 4,
                    name: "Level 4",
                    nextStage: "Level 5",
                    description: "Meet your serves goal for 6 days this week!",
                    current: false
                },
                {
                    number: 5,
                    name: "Level 5",
                    nextStage: "",
                    description: "Meet your serves goal for all days this week!",
                    current: false
                }
            ];

        firebaseRef.child(currentUser).once('value').then(function (snapshot) {
            initializeProgressBar(firebaseRef, currentUser, $ionicPopup, $scope.stages);
            $scope.rewardsNotSet = (snapshot.child('stageReward').val() == null);
            if ($scope.rewardsNotSet) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Set Rewards!',
                    template: 'Lets set some rewards for different levels?'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        $state.go('rewards');
                    }
                });
            }

        }, function (error) {
            console.log(error);
        });

        initializeCarrot(firebaseRef, currentUser, today, today, $scope.dateCreated);
        initializeEditGoalBtn(firebaseRef, currentUser, GetTodayService.ddmmyyyy());

        $scope.setGoal = function () {
            var setGoalPopup = $ionicPopup.show({
                template: '<input type="number" ng-model="data.newGoal" placeholder="e.g. 4">',
                title: 'Set Daily Serves Goal',
                subTitle: "Recommended: 6 Serves - Men <br /> 5 Serves - Women",
                scope: $scope,
                buttons: [
                    {text: 'Cancel'},
                    {
                        text: '<b>Set</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            if (!$scope.data.newGoal) {
                                e.preventDefault();

                            } else {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'New Goal Set',
                                    template: 'Daily Goal has been updated'
                                });

                                var today = GetTodayService.ddmmyyyy();
                                firebaseRef.child(currentUser).child("userDetails").update({
                                    dailyServesGoal: $scope.data.newGoal
                                });

                                firebaseRef.child(currentUser).child("statistic").child(today).update({
                                    goal: $scope.data.newGoal
                                });
                            }
                        }
                    }
                ]
            });
        };

        $scope.showLevelsInfo = function () {
            var alertPopup = $ionicPopup.alert({
                title: 'Level Information',
                template: 'Level 1:Signup and meet your serves goal for 1 day in a week<br/>' +
                'Level 2:Meet your serves goal for 2 days in a week<br/>' +
                'Level 3:Meet your serves goal for 4 days in a week<br/>' +
                'Level 4:Meet your serves goal for 6 days in a week<br/>' +
                'Level 5:Meet your serves goal for 7 days in a week'
            });
        };

    })

    .controller('RewardCtrl', function ($rootScope, $scope, $ionicPopup, $state) {
        var firebaseRef = $rootScope.firebaseRef;
        var currentUser = localStorage.getItem("currentUser");
        var myPopup;
        $scope.data = {};
        $scope.rewards = [];
        $scope.visibility = {reward: "block"};
        $scope.numStages = 5;

        $scope.stage1 =
            [
                {text: "An Episode of a TV Show"},
                {text: "An Afternoon in the Sun"}
            ];

        $scope.stage2 =
            [
                {text: "A New Book"},
                {text: "A New Pair of Socks"}
            ];

        $scope.stage3 =
            [
                {text: "A New T-Shirt"},
                {text: "Movie Tickets"}
            ];

        $scope.stage4 =
            [
                {text: "New Watch"},
                {text: "New Bag"}
            ];

        $scope.stage5 =
            [
                {text: "A Haircut"},
                {text: "A Travel"}
            ];


        //Reads user added rewards from Firebase and pushes it to $scope.rewards
        $scope.renderNewRewards = function () {
            console.log("renderNewRewards");
            firebaseRef.child(currentUser).once("value", function (snapshot) {
                if (snapshot.child("reward").val() != null) {
                    var length = snapshot.child("reward").val().length;

                    for (var i = 0; i < length; i++) {
                        var reward = snapshot.child("reward").child(i).val();
                        $scope.rewards.push({text: reward.text});
                    }
                }

            });
        };


        /*
         * Reads the 5 rewards the user has set for him/herself from
         * Firebase and marks those as "selected" in the 5 dropdowns
         * on selectReward.html
         */
        $scope.displaySelectedRewards = function () {
            console.log("displaySelectedRewards");
            firebaseRef.child(currentUser).on("value", function (snapshot) {
                for (var i = 1; i <= $scope.numStages; i++) {
                    var rewardName = snapshot.child("stageReward").child(i + "").child("content").val();
                    var dropDown = document.getElementById("stage" + i);
                    
                    for (var j = 0; j < dropDown.options.length; j++) {
                        var name = dropDown.options[j].text;

                        if (name == rewardName) {
                            dropDown.options[j].selected = "selected";
                            break;
                        }
                    }
                }
            });
        };


        /*
         * When a user adds his/her own reward
         * Check if Reward with same name exists.
         * Alert User if it does.
         * Else, add the new reward and reset the default input.
         */
        $scope.addReward = function () {
            myPopup = $ionicPopup.show({
                template: '<input type="text" ng-model="data.reward">',
                title: 'New Reward',
                scope: $scope,
                buttons: [
                    {text: 'Cancel'},
                    {
                        text: '<b>Create</b>',
                        type: 'button-balanced',
                        onTap: function (e) {
                            if (!$scope.data.reward) {
                                e.preventDefault();
                            } else {
                                var duplicated = false;
                                for (var i = 0; i < $scope.rewards.length; i++) {
                                    if ($scope.rewards[i].text.toUpperCase() == $scope.data.reward.toUpperCase()) {
                                        duplicated = true;
                                    }
                                }

                                if (duplicated) {
                                    var alertPopup = $ionicPopup.alert({
                                        title: 'Reward Exists!',
                                        template: 'Reward "' + $scope.data.reward + '" has already existed!'
                                    });
                                } else {
                                    $scope.rewards.push({text: $scope.data.reward});
                                    $scope.data.reward = '';
                                }
                            }
                        }
                    }
                ]
            });
        };

        $scope.deleteReward = function (text) {
            for (var i = 0; i < $scope.rewards.length; i++) {
                if ($scope.rewards[i].text == text) {
                    $scope.rewards.splice(i, 1);
                }
            }
        };

        //Populates stageReward with the content of the reward for each stage
        $scope.getStageReward = function (stageReward, stage) {
            var currentStage = document.getElementById("stage" + stage);
            var value = currentStage.options[currentStage.selectedIndex].text;
            if (value != "Choose from the following:") {
                stageReward[stage]["content"] = value;
            }
        };

        //Gets rewards the user has set and pushes them to the database
        $scope.confirm = function () {
            var stageReward = {
                1: {content: ""},
                2: {content: ""},
                3: {content: ""},
                4: {content: ""},
                5: {content: ""}
            };
            for (var i = 1; i <= $scope.numStages; i++) {
                $scope.getStageReward(stageReward, i);
            }

            //Gets user added rewards and pushes them to the database
            var rewards = {};
            for (var j = 0; j < $scope.rewards.length; j++) {
                var reward = $scope.rewards[j];
                rewards[j + ""] = {};
                rewards[j + ""]["text"] = reward.text;
            }

            firebaseRef.child(currentUser).child("stageReward").set(stageReward, function (error) {
                firebaseRef.child(currentUser).child("reward").set(rewards, function (error) {
                    $state.go('tab.dashboard');
                });
            });
        }
    })

    .controller('UpdateCtrl', function ($rootScope, $scope, $ionicPopup, $state, GetTodayService) {
        var firebaseRef = $rootScope.firebaseRef;
        var currentUser = localStorage.getItem("currentUser");

        $scope.dateCreated;
        firebaseRef.child(currentUser).once("value", function (snapshot) {
            $scope.dateCreated = (snapshot.child('userDetails').child('startDate').val());
        });

        var today = GetTodayService.ddmmyyyy();
        $scope.data = {};
        $scope.data.mealTime;
        $scope.data.dateString;

        $scope.vegetableType = [
            {text: "Green and Leafy Green Vegetables", value: "green", checked: false},
            {text: "Peas, Beans and Sprouts", value: "peas", checked: false},
            {text: "Potatoes", value: "potatoes", checked: false},
            {text: "Carrots and Root Vegetables", value: "root", checked: false},
            {text: "Cabbage, Cauliflower and Brassica Vegetables", value: "brassica", checked: false},
            {text: "Other Fruiting Vegetables", value: "other", checked: false}
        ];

        var referenceDate = new Date(today);
        $scope.data.dateString = today;

        initializeDiary(today);

        $scope.decreaseDiaryIndex = function () {
            resetDiary();
            var leastDate = new Date($scope.dateCreated);
            if (referenceDate > leastDate) {
                referenceDate.setDate(referenceDate.getDate() - 1);
                var month = (referenceDate.getMonth() + 1);
                if (month < 10) month = "0" + month;
                var date = referenceDate.getDate();
                if (date < 10) date = "0" + date;
                var year = referenceDate.getFullYear();
                $scope.data.dateString = year + "-" + month + "-" + date;
                initializeDiary($scope.data.dateString);
            }
        };

        $scope.increaseDiaryIndex = function () {
            resetDiary();
            var maxDate = new Date(today);
            if (referenceDate < maxDate) {
                referenceDate.setDate(referenceDate.getDate() + 1);
                var month = (referenceDate.getMonth() + 1);
                if (month < 10) month = "0" + month;
                var date = referenceDate.getDate();
                if (date < 10) date = "0" + date;
                var year = referenceDate.getFullYear();
                $scope.data.dateString = year + "-" + month + "-" + date;
                initializeDiary($scope.data.dateString);
            }
        };

        function initializeDiary(date) {
            var userObject = firebaseRef.child(currentUser).child("statistic");
            userObject.on("value", function (snapshot) {
                $scope.data.serve = snapshot.child(date).child("servesEaten").val();
            });

            if (date == today) {
                document.getElementById("diaryChevronRight").disabled = true;
                document.getElementById("diaryDate").innerHTML = "Today";
                //if (today == localStorage.getItem('dateCreated')) {
                if (today == $scope.dateCreated) {
                    document.getElementById("diaryChevronLeft").disabled = true;
                }
                //} else if (date == localStorage.getItem('dateCreated')) {
            } else if (date == $scope.dateCreated) {
                document.getElementById("diaryChevronLeft").disabled = true;
                document.getElementById("diaryDate").innerHTML = (new Date(date)).toDateString().substring(0, 15);
            } else {
                document.getElementById("diaryChevronLeft").disabled = false;
                document.getElementById("diaryChevronRight").disabled = false;
                document.getElementById("diaryDate").innerHTML = (new Date(date)).toDateString().substring(0, 15);
            }

        }

        function resetDiary() {
            $scope.data.mealTime = "";
            document.getElementById("btnBreakfast").style.backgroundColor = "transparent";
            document.getElementById("btnLunch").style.backgroundColor = "transparent";
            document.getElementById("btnDinner").style.backgroundColor = "transparent";
            document.getElementById("btnSnack").style.backgroundColor = "transparent";

            document.getElementById("updateIntakeBtn").style.visibility = "hidden";

            $scope.vegetableType = [
                {text: "Green and Leafy Green Vegetables", value: "green", checked: false},
                {text: "Peas, Beans and Sprouts", value: "peas", checked: false},
                {text: "Potatoes", value: "potatoes", checked: false},
                {text: "Carrots and Root Vegetables", value: "root", checked: false},
                {text: "Cabbage, Cauliflower and Brassica Vegetables", value: "brassica", checked: false},
                {text: "Other Fruiting Vegetables", value: "other", checked: false}
            ];

            $scope.data.serve = null;
        }


        $scope.showMealInfo = function (meal) {
            $scope.data.mealTime = meal;
            document.getElementById("btnBreakfast").style.backgroundColor = "transparent";
            document.getElementById("btnLunch").style.backgroundColor = "transparent";
            document.getElementById("btnDinner").style.backgroundColor = "transparent";
            document.getElementById("btnSnack").style.backgroundColor = "transparent";
            document.getElementById("btn" + meal).style.backgroundColor = "#66ccff";
            document.getElementById("updateIntakeBtn").style.visibility = "visible";

            var userObject = firebaseRef.child(currentUser).child("statistic");
            userObject.once("value", function (snapshot) {
                var mealsPerDate = snapshot.child($scope.data.dateString).child(meal);
                var brassica = mealsPerDate.child("brassica").val();
                var green = mealsPerDate.child("green").val();
                var other = mealsPerDate.child("other").val();
                var peas = mealsPerDate.child("peas").val();
                var potatoes = mealsPerDate.child("potatoes").val();
                var root = mealsPerDate.child("root").val();

                if (!brassica) {
                    brassica = false;
                }

                if (!green) {
                    green = false;
                }

                if (!other) {
                    other = false;
                }

                if (!peas) {
                    peas = false;
                }

                if (!potatoes) {
                    potatoes = false;
                }

                if (!root) {
                    root = false;
                }

                $scope.vegetableType = [
                    {
                        text: "Green and Leafy Green Vegetables",
                        value: "green",
                        checked: green
                    },
                    {
                        text: "Peas, Beans and Sprouts",
                        value: "peas",
                        checked: peas
                    },
                    {
                        text: "Potatoes",
                        value: "potatoes",
                        checked: potatoes
                    },
                    {
                        text: "Carrots and Root Vegetables",
                        value: "root",
                        checked: root
                    },
                    {
                        text: "Cabbage, Cauliflower and Brassica Vegetables",
                        value: "brassica",
                        checked: brassica
                    },
                    {
                        text: "Other Fruiting Vegetables",
                        value: "other",
                        checked: other
                    }
                ];

                $scope.data.serve = mealsPerDate.child("serve").val();
                if (mealsPerDate.child("serve").val() == null) {
                    $scope.data.serve = 0;
                }

            })
        };

        $scope.updateIntake = function () {

            var confirmPopup = $ionicPopup.confirm({
                title: "Confirm Update",
                template: 'Update serves had at ' + $scope.data.mealTime + ' to ' + $scope.data.serve + '?',
                okType: 'button-positive'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    if ($scope.data.serve != null && $scope.data.serve >= 0) {
                        confirmIntake(firebaseRef, currentUser,
                            $scope.data.dateString,
                            $scope.data.mealTime,
                            $scope.data.serve,
                            $scope.vegetableType,
                            true,
                            $state
                        );

                        resetDiary();

                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Log Vegetables!',
                            template: 'Please log some vegetables before confirming!'
                        });
                    }
                }
            });
        };

        $scope.servesInformation = function () {
            $state.go('vegetableTypes');
        };

    })

    .controller('LogCtrl', function ($rootScope, $scope, $state, $ionicPopup, GetTodayService) {
        var firebaseRef = $rootScope.firebaseRef;
        var currentUser = localStorage.getItem("currentUser");

        $scope.mealTime = [
            {text: "Breakfast", value: "Breakfast"},
            {text: "Lunch", value: "Lunch"},
            {text: "Dinner", value: "Dinner"},
            {text: "Snack", value: "Snack"}
        ];

        $scope.vegetableType = [
            {text: "Green and Leafy Green Vegetables", value: "green", checked: false},
            {text: "Peas, Beans and Sprouts", value: "peas", checked: false},
            {text: "Potatoes", value: "potatoes", checked: false},
            {text: "Carrots and Root Vegetables", value: "root", checked: false},
            {text: "Cabbage, Cauliflower and Brassica Vegetables", value: "brassica", checked: false},
            {text: "Other Fruiting Vegetables", value: "other", checked: false}
        ];

        $scope.data = {};
        $scope.displayIndex = 0;
        var today = GetTodayService.ddmmyyyy();

        $scope.confirmIntakes = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: "Confirm Intake",
                template: 'Are you sure you to log ' + $scope.data.serve + ' serve(s) for ' + $scope.data.mealTime + '?',
                okType: 'button-positive'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    if ($scope.data.serve != null && $scope.data.serve > 0) {
                        confirmIntake(firebaseRef, currentUser, today,
                            $scope.data.mealTime,
                            $scope.data.serve,
                            $scope.vegetableType,
                            false,
                            $state
                        );

                        $scope.data = {};
                        $scope.vegetableType = [
                            {text: "Green and Leafy Green Vegetables", value: "green", checked: false},
                            {text: "Peas, Beans and Sprouts", value: "peas", checked: false},
                            {text: "Potatoes", value: "potatoes", checked: false},
                            {text: "Carrots and Root Vegetables", value: "root", checked: false},
                            {text: "Cabbage, Cauliflower and Brassica Vegetables", value: "brassica", checked: false},
                            {text: "Other Fruiting Vegetables", value: "other", checked: false}
                        ];

                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Log Vegetables!',
                            template: 'Please log some vegetables before confirming!'
                        });
                    }
                }
            });
        };


        $scope.servesInformation = function () {
            $state.go('vegetableTypes');
        };

    })

    .controller('VegetableTypesCtrl', function ($scope, $rootScope) {
        $scope.toggleShow = function (vegId) {
            var div = document.getElementById(vegId);
            if (div.className !== 'hidden') {
                div.className = 'hidden';
            }
            else {
                div.className = 'show';
            }
        }
    })

    .controller('HistoryCtrl', function ($rootScope, $scope) {
        var firebaseRef = $rootScope.firebaseRef;
        var currentUser = localStorage.getItem("currentUser");

        $scope.renderCharts = function () {
            $scope.barData = {
                labels: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                datasets: [
                    {
                        label: 'Weekly Intake',
                        fillColor: 'rgba(151,187,205,0.5)',
                        strokeColor: 'rgba(151,187,205,0.8)',
                        highlightFill: 'rgba(151,187,205,0.75)',
                        highlightStroke: 'rgba(151,187,205,1)',
                        data: [0, 0, 0, 0, 0, 0, 0]
                    }
                ]
            };

            var xAxisDates = new Array();
            for (var i = 0; i < 7; i++) {
                var dateContainer = new Date();
                dateContainer.setDate(dateContainer.getDate() - (6 - i));
                var mmdd_date = dateContainer.getMonth() + 1 + "/" + dateContainer.getDate();
                var dateString = mmdd_date.toString();
                xAxisDates.push(dateString);
            }

            $scope.barData.labels = xAxisDates;

            var barDataBuffer = {
                label: 'Weekly Intake',
                fillColor: 'rgba(151,187,205,0.5)',
                strokeColor: 'rgba(151,187,205,0.8)',
                highlightFill: 'rgba(151,187,205,0.75)',
                highlightStroke: 'rgba(151,187,205,1)',
                data: [0, 0, 0, 0, 0, 0, 0]
            };

            var currentServe = 0;

            firebaseRef.child(currentUser).child("statistic").on('value', function (snapshot) {
                for (var i = 6; i >= 0; i--) {

                    var date = getDateDaysFromToday(i);
                    currentServe = snapshot.child(date).child('servesEaten').val();

                    if (currentServe == null) {
                        currentServe = 0;
                    }

                    barDataBuffer.data[6 - i] = currentServe;
                    $scope.barData.datasets[0] = barDataBuffer;
                }
            });

            $scope.barOptions = {

                // Sets the chart to be responsive
                responsive: true,

                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: true,

                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,

                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",

                //Number - Width of the grid lines
                scaleGridLineWidth: 1,

                //Boolean - If there is a stroke on each bar
                barShowStroke: true,

                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,

                //Number - Spacing between each of the X value sets
                barValueSpacing: 10,

                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,

                //String - A legend template
                legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
            };

            $scope.lineData = {
                labels: ['Week1', 'Week2', 'Week3', 'Week4'],
                datasets: [
                    {
                        label: 'Weekly Goal',
                        fillColor: 'rgba(255,255,255,0.4)',
                        strokeColor: 'rgba(211,247,208,0.8)',
                        pointColor: 'rgba(164,224,185,1)',
                        pointStrokeColor: '#fff',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(164,224,185,1)',
                        data: []
                    },
                    {
                        label: 'Weekly Intake',
                        fillColor: 'rgba(151,187,205,0.2)',
                        strokeColor: 'rgba(151,187,205,1)',
                        pointColor: 'rgba(151,187,205,1)',
                        pointStrokeColor: '#fff',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(151,187,205,1)',
                        data: []
                    },
                    {
                        label: 'Weekly Average',
                        fillColor: 'rgba(255,255,255,0.4)',
                        strokeColor: 'rgba(255,138,138,0.8)',
                        pointColor: 'rgba(208,100,100,1)',
                        pointStrokeColor: '#fff',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(208,100,100,1)',
                        data: []
                    }
                ]
            };

            firebaseRef.child(currentUser).on('value', function (snapshot) {
                var signUpDate = snapshot.child("userDetails").child("startDate").val();
                var startDate = new Date(signUpDate);
                var msPerDay = 8.64e7;
                var endDate = new Date(); //Today
                startDate.setHours(12, 0, 0);
                endDate.setHours(12, 0, 0);
                var numDays = Math.round((endDate - startDate) / msPerDay) + 1;
                var numWeeks = Math.round(numDays / 7);

                var difference = numDays - numWeeks * 7;
                //console.log(difference);

                var weeklyIntakeSum = 0;
                var weeklyGoalSum = 0;

                for (var i = (numWeeks * 7) - 1; i >= 0; i--) {

                    var dateString = getDateDaysFromToday(i + difference);
                    //console.log(i + "-" + dateString);

                    var dailyIntake = snapshot.child("statistic").child(dateString).child('servesEaten').val();
                    if (dailyIntake == null) {
                        dailyIntake = 0;
                    }
                    weeklyIntakeSum += dailyIntake;

                    var dailyGoal = snapshot.child("statistic").child(dateString).child('goal').val();
                    if (dailyGoal == null) {
                        dailyGoal = snapshot.child("userDetails").child("dailyServesGoal").val();
                    }
                    weeklyGoalSum += dailyGoal;

                    var seventhDayFlag = i % 7;
                    if (seventhDayFlag == 0) {
                        var weekIdx = numWeeks - 1 - i / 7;
                        $scope.lineData.datasets[0].data[weekIdx] = weeklyGoalSum;
                        $scope.lineData.datasets[1].data[weekIdx] = weeklyIntakeSum;
                        $scope.lineData.datasets[2].data[weekIdx] = Math.round(weeklyIntakeSum / 7);
                        weeklyGoalSum = 0;
                        weeklyIntakeSum = 0;
                    }
                }

            });

            $scope.lineOptions = {

                // Sets the chart to be responsive
                responsive: true,

                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,

                //String - Colour of the grid lines
                scaleGridLineColor: 'rgba(0,0,0,.05)',

                //Number - Width of the grid lines
                scaleGridLineWidth: 1,

                //Boolean - Whether the line is curved between points
                bezierCurve: false,

                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.4,

                //Boolean - Whether to show a dot for each point
                pointDot: true,

                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,

                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,

                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,

                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,

                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,

                //Boolean - Whether to fill the dataset with a colour
                datasetFill: true,

                //String - A legend template
                legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
            };
        };

        function getDateDaysFromToday(difference) {
            var today = new Date();
            today.setDate(today.getDate() - difference);
            var month = (today.getMonth() + 1);
            if (month < 10) month = "0" + month;
            var date = today.getDate();
            if (date < 10) date = "0" + date;
            var dateString = today.getFullYear() + "-" + month + "-" + date;
            return dateString;
        }

        $scope.showSevenDaysIntakeInfo = function () {
            var popup = $ionicPopup.alert({
                title: 'Weekly Progress',
                template: 'Shows the serves of vegetables had over the past seven days'
            })

        };

        $scope.showBarGraph = function () {
            var div = document.getElementById('barGraphCanvas');
            if (div.className !== 'hidden') {
                div.className = 'hidden';
            }
            else {
                div.className = 'show';
            }

        };

        $scope.showLineGraph = function () {
            var div = document.getElementById('lineGraphCanvas');
            if (div.className !== 'hidden') {
                div.className = 'hidden';
            }
            else {
                div.className = 'show';
            }
        };
    })

    .controller('AccountCtrl', function ($rootScope, $scope, $ionicPopup, $state) {
        var firebaseRef = $rootScope.firebaseRef;
        $scope.data = {};

        $scope.changePassword = function () {
            var myPopup = $ionicPopup.show({
                template: '<div>' +
                '<input type="email" placeholder="Email" ng-model="data.email">' +
                '<input type="password" placeholder="Old Password" ng-model="data.oldPassword">' +
                '<input type="password" placeholder="New Password" ng-model="data.newPassword">' +
                '<input type="password" placeholder="Retype Password" ng-model="data.newPasswordRepeat"></div>',
                title: 'Change Password',
                scope: $scope,
                buttons: [
                    {text: 'Cancel'},
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            if (!$scope.data.oldPassword || !$scope.data.newPassword || !$scope.data.newPasswordRepeat) {
                                e.preventDefault();
                                var errPopupA = $ionicPopup.alert({
                                    title: 'All fields needed',
                                    template: 'Please enter all three fields to continue'
                                })
                            } else {
                                if ($scope.data.newPassword != $scope.data.newPasswordRepeat) {
                                    var errPopupB = $ionicPopup.alert({
                                        title: 'Retype Passwords',
                                        template: 'New Passwords do not match!'
                                    })
                                }
                                firebaseRef.changePassword({
                                    email: $scope.data.email,
                                    oldPassword: $scope.data.oldPassword,
                                    newPassword: $scope.data.newPassword
                                }, function (error) {
                                    if (error) {
                                        switch (error.code) {
                                            case "INVALID_PASSWORD":
                                                console.log("The specified user account password is incorrect.");
                                                var errPopupC = $ionicPopup.alert({
                                                    title: 'Try again!',
                                                    template: 'The specified user account password is incorrect'
                                                });
                                                break;
                                            case "INVALID_USER":
                                                console.log("The specified user account does not exist.");
                                                var errPopupD = $ionicPopup.alert({
                                                    title: 'Try again!',
                                                    template: 'The specified user account does not exist'
                                                });
                                                break;
                                            default:
                                                console.log("Error changing password:", error);
                                        }
                                    } else {
                                        console.log("User password changed successfully!");
                                        var errPopupE = $ionicPopup.alert({
                                            title: 'Success!',
                                            template: 'User password changed successfully!'
                                        })
                                    }
                                });
                            }
                        }
                    }
                ]
            });


        };

        $scope.logout = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Logout',
                template: 'Are you sure to log out?',
                okType: 'button-positive'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    localStorage.removeItem("currentUser");
                    $state.go('login');
                }
            });
        }
    })

    .controller('AdminCtrl', function ($scope, $ionicPopup, $state) {
        $scope.logout = function () {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Logout?',
                template: 'Are you sure to log out',
                okType: 'button-positive'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    localStorage.removeItem("currentUser");
                    $state.go('login');
                }
            });
        }
    });


