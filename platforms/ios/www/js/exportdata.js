function exportUserDetails() {
    var ref = new Firebase('https://vege-tracker.firebaseio.com');

    var username, dob, email, age, gender, currentStage, dailyServesGoal, timesGoalReached;
    var now = new Date();
    var currentYear = now.getFullYear();
    var data = [];
    var columns = ["uid", "username", "dob", "email", "age", "gender", "currentStage", "dailyServesGoal", "timesGoalReached"];
    data.push(columns);
    ref.once('value', function (snapshot) {
        snapshot.forEach(function (func) {
            var csvEntry = [];
            var uid = func.key();
            username = func.child("userDetails").child("username").val();
            dob = func.child("userDetails").child("dob").val();
            email = func.child("userDetails").child("email").val();
            age = currentYear - new Date(dob).getFullYear();
            gender = func.child("userDetails").child("gender").val();
            currentStage = func.child("userDetails").child("currentStage").val();
            dailyServesGoal = func.child("userDetails").child("dailyServesGoal").val();
            timesGoalReached = func.child("userDetails").child("timesGoalReached").val();
            csvEntry.push(uid);
            csvEntry.push(username);
            csvEntry.push(dob);
            csvEntry.push(email);
            csvEntry.push(age);
            csvEntry.push(gender);
            csvEntry.push(currentStage);
            csvEntry.push(dailyServesGoal);
            csvEntry.push(timesGoalReached);
            data.push(csvEntry);
        });
        var file = convertArrayOfObjectsToCSV({data: data});
        downloadCSV({filename: "VegeTracker-UserDetails.csv"}, file);
    })
}

function exportUserStatistics() {
    var ref = new Firebase('https://vege-tracker.firebaseio.com');
    var username, uid;
    var data = [];
    var columns =
        [
            "uid", "username", "date", "goal", "reachGoal", "servesEaten", "mealtime",
            "serve", "brassica", "green", "potatoes", "peas", "other", "root", "time"
        ];
    data.push(columns);

    ref.once('value', function (snapshot) {

        snapshot.forEach(function (userObj) {

            var statsObj = userObj.child("statistic").val();
            username = userObj.child("userDetails").child("username").val();
            uid = userObj.key();
            var csventry = [];

            if (statsObj == null) {
                csventry.push(uid);
                csventry.push(username);
                data.push(csventry);

            } else {
                for (var eachDay in statsObj) {
                    var goal = statsObj[eachDay]["goal"];
                    var reachGoal = statsObj[eachDay]["reachGoal"];
                    var servesEaten = statsObj[eachDay]["servesEaten"];

                    if (statsObj[eachDay] == null) {
                        csventry = [];
                        csventry.push(uid);
                        csventry.push(username);
                        csventry.push(eachDay);
                        data.push(csventry);

                    } else {
                        for (var item in statsObj[eachDay]) {
                            var meals = ["Breakfast", "Lunch", "Dinner", "Snack"];
                            var irrelevent = ["goal", "reachGoal", "servesEaten"];
                            if ((meals.indexOf(item) > -1) && (irrelevent.indexOf(item) == -1)) {
                                csventry = [];
                                csventry.push(uid);
                                csventry.push(username);
                                csventry.push(eachDay);
                                csventry.push(goal);
                                csventry.push(reachGoal);
                                csventry.push(servesEaten);
                                csventry.push(item);
                                csventry.push(statsObj[eachDay][item]["serve"]);
                                csventry.push(statsObj[eachDay][item]["brassica"]);
                                csventry.push(statsObj[eachDay][item]["green"]);
                                csventry.push(statsObj[eachDay][item]["potatoes"]);
                                csventry.push(statsObj[eachDay][item]["peas"]);
                                csventry.push(statsObj[eachDay][item]["other"]);
                                csventry.push(statsObj[eachDay][item]["root"]);

                                var systemtime = statsObj[eachDay][item].time;
                                var date = new Date(systemtime);
                                var hours = date.getHours();
                                var minutes = "0" + date.getMinutes();
                                var seconds = "0" + date.getSeconds();
                                var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

                                csventry.push(formattedTime);
                                data.push(csventry);
                            }
                        }
                    }
                }
            }
        });
        var file = convertArrayOfObjectsToCSV({data: data});
        downloadCSV({filename: "statistic-data.csv"}, file);
    });
}

function convertArrayOfObjectsToCSV(args) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;
    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';
    keys = Object.keys(data[0]);
    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function (item) {
        ctr = 0;
        keys.forEach(function (key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
}

function downloadCSV(args, csv) {
    var data, filename, link;
    if (csv == null){
        return;
    }
    filename = args.filename || 'data.csv';
    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);
    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
}