//Initialize all the elements on the home screen

function initializeCarrot(firebaseRef, currentUser, date, today, dateCreated) {
    var userObject = firebaseRef.child(currentUser);
    userObject.on("value", function (snapshot) {

        //Get Statistics -> today -> goal
        var dailyServesGoal = snapshot.child("statistic").child(date).child("goal").val();
        if (dailyServesGoal == null) {
            dailyServesGoal = snapshot.child("userDetails").child("dailyServesGoal").val();
        }

        //Get Statistics -> today -> servesEaten
        var servesEatenToday = snapshot.child("statistic").child(date).child("servesEaten").val();
        if (servesEatenToday == null) {
            servesEatenToday = 0;
        }

        //Calculate the percentage for carrot
        var percentGoalAchieved = Math.floor(servesEatenToday * 100 / dailyServesGoal);

        //Carrot Meter
        //Set the height of the white box around the carrot
        var whiteBox = document.getElementById('white');
        if (percentGoalAchieved > 100) percentGoalAchieved = 100;
        whiteBox.style.height = (299 - 222 * percentGoalAchieved / 100) + "px";

        //Set the Carrot Meter with the latest details
        var current = document.getElementById('currentServe');
        current.innerHTML = servesEatenToday + "/" + dailyServesGoal + "</br>" + (percentGoalAchieved + "%");

        // if (date == today) {
        //     document.getElementById("chevronRight").disabled = true;
        //     document.getElementById("carrotDate").innerHTML = "Today";
        //     //if (today == localStorage.getItem('dateCreated')) {
        //     if (today == dateCreated) {
        //         document.getElementById("chevronLeft").disabled = true;
        //     }
        //
        // //} else if (date == localStorage.getItem('dateCreated')) {
        // } else if (date == dateCreated) {
        //     document.getElementById("chevronLeft").disabled = true;
        //     document.getElementById("carrotDate").innerHTML = (new Date(date)).toDateString().substring(0, 15);
        //
        // } else {
        //     document.getElementById("chevronLeft").disabled = false;
        //     document.getElementById("chevronRight").disabled = false;
        //     document.getElementById("carrotDate").innerHTML = (new Date(date)).toDateString().substring(0, 15);
        //
        // }
    })
}

function initializeEditGoalBtn(firebaseRef, currentUser, today) {
    var userObject = firebaseRef.child(currentUser);

    userObject.child("userDetails").child("dailyServesGoal").on("value", function (snapshot) {
        var dailyServesGoal = snapshot.val();
        var goalServe = document.getElementById('goalServe');
        goalServe.innerHTML = "Goal: " + dailyServesGoal + " Serve(s)";
    })
}

function initializeProgressBar(firebaseRef, currentUser, ionicPopup, stages) {
    var userObject = firebaseRef.child(currentUser);

    userObject.on("value", function (snapshot) {
        //Stages & Progress Bar
        var currentStreak = snapshot.child("userDetails").child("timesGoalReached").val();
        var currentStage = snapshot.child("userDetails").child("currentStage").val();

        var progressPercentage;
        var progressNumDays;
        var outOfDays;

        var numOfStages = stages.length;
        var currentStageName = stages[currentStage - 1].name;
        var nextStageName = stages[currentStage - 1].nextStage;
        var stageDescription = stages[currentStage - 1].description;
        var stageReward = snapshot.child("stageReward").child(currentStage).child("content").val();

        if (currentStage == 1) {
            if (currentStreak == 0) {
                progressPercentage = 50;
                progressNumDays = 0;
                outOfDays = 1;
            }
            else if (currentStreak >= 1) {
                progressPercentage = 1;
                progressNumDays = 1;
                outOfDays = 1;
                updateStage(userObject, 2, ionicPopup, stageReward);
            }
        }

        if (currentStage == 2) {
            if (currentStreak >= 3) {
                updateStage(userObject, 3, ionicPopup);
            }
            progressPercentage = 100 * ((currentStreak - 1) / 2);
            progressNumDays = currentStreak - 1;
            outOfDays = 2;

        }

        if (currentStage == 3) {
            if (currentStreak >= 7) {
                updateStage(userObject, 4, ionicPopup, stageReward);
            }
            progressPercentage = ((currentStreak - 3) / 4) * 100;
            progressNumDays = (currentStreak - 3);
            outOfDays = 4;

        }

        if (currentStage == 4) {
            if (currentStreak >= 13) {
                //progressPercentage = 100;
                updateStage(userObject, 5, ionicPopup, stageReward);
            }
            progressPercentage = ((currentStreak - 7) / 6) * 100;
            progressNumDays = (currentStreak - 7);
            outOfDays = 6;

        }

        if (currentStage === 5) {
            if (currentStreak >= 20) {
                updateStage(userObject, 6, ionicPopup, stageReward);
            }
            progressPercentage = ((currentStreak - 13) / 7) * 100;
            progressNumDays = (currentStreak - 13);
            outOfDays = 7;

        }

        if (currentStage > numOfStages) {
            progressPercentage = 100;
            progressNumDays = 7;
            outOfDays = 7;
            stageReward = "Isn't being here a reward in itself?";
            currentStageName = "";
            nextStageName = "";
            stageDescription = "All levels are complete! Keep it up!";

        }

        //Progress Bar Markers
        document.getElementById("currentStage").innerHTML = currentStageName;
        document.getElementById("nextStage").innerHTML = nextStageName;

        //Progress Bar
        document.getElementById("percentage").innerHTML = "<strong>" + progressNumDays + "/" +
            outOfDays + " day(s) </strong>";

        document.getElementById("currentBar").style.width = progressPercentage + "%";

        //Goal Description
        document.getElementById("stageDescription").innerHTML = stageDescription;

        //Reward
        document.getElementById("stageReward").innerHTML = stageReward;

        //Level Visualiser
        document.getElementById("levelVisualiser").src = "img/" + currentStage + ".png";

    })
}

function updateStage(user, newStage, ionicPopup, stageReward) {
    user.child("userDetails").update({
        currentStage: newStage
    }, function (error) {
        if (error) {
            console.log(error);
        } else {
            var congoPopup = ionicPopup.alert({
                title: 'Congratulations!',
                template: 'You have now progressed onto Level '+newStage+'! Please claim your reward: <br /><strong>' + stageReward + '</strong>',
                okType: 'button-assertive'

            });
            congoPopup.then(function (res) {
                if (res) {
                    location.reload();
                }
            });

        }
    });
}