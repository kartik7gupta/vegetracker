angular.module('starter.services', [])

    .factory('GetTodayService', function () {
        var today = new Date();

        var month = (today.getMonth() + 1);
        if (month < 10) month = "0" + month;

        var date = today.getDate();
        if (date < 10) date = "0" + date;

        var year = today.getFullYear();

        //TODO Correct Format
        var today_ddmmyyyy = year + "-" + month + "-" + date;
        //var today_ddmmyyyy =  date + "-" + month + "-" + today.getFullYear() ;

        return {
            ddmmyyyy: function () {
                return today_ddmmyyyy;
            },
            getDate: function(){
                return today.getDate();
            },
            getMonth: function(){
                return today.getMonth();
            },
            getYear: function () {
                return today.getFullYear();
            },
            getDefaultDate: function(){
                return today;
            }
        };
    });

